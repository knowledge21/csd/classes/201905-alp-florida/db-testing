﻿using System;
using System.Collections.Generic;
using System.Text;
using Gallio.Framework;
using NUnit.Framework;
using NHibernate;
using SalesSystem.Business;
using SalesSystem.Model;
using SalesSystem;

namespace SalesSystem.Tests
{ 
    [TestFixture]
    public class SaleTests : TesteBase
    {
        private ISession _session;
        
        [SetUp]
        public void Initialize()
        {
            _session = Context.SessionFactory.OpenSession();
        }

        [Test]
        public void TestSalesmanOne()
        {
            var year = 1969;
            var salesmanId = 1;
            ReportGenerator gr = new ReportGenerator();
            Assert.AreEqual(300, gr.TotalYearSalesBySalesman(year, salesmanId, _session));
        }

        [Test]
        public void TestSalesmanTwo()
        {
            var year = 1981;
            var salesmanId = 200;
            ReportGenerator gr = new ReportGenerator();
            Assert.AreEqual(10000, gr.TotalYearSalesBySalesman(year, salesmanId, _session));
        }

        [TearDown]
        public void TestSalesman3()
        {
            var year = 2019;
            var salesmanId = 300;
            ReportGenerator gr = new ReportGenerator();
            Assert.AreEqual(110000, gr.TotalYearSalesBySalesman(year, salesmanId, _session));
        }


    }
}
