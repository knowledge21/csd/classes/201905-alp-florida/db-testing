﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace SalesSystem.Business
{
    public class ReportGenerator
    {
        public double TotalYearSalesBySalesman(int year, int salesmanId, ISession _session)
        {

            string query = "select sum(v.value) from Sale v where v.salesmanId = :salesmanId and YEAR(v.saleDate) = :saleYear";

            double salesValue = _session.CreateSQLQuery(query)
                .SetParameter("salesmanId", salesmanId)
                .SetParameter("saleYear", year)
                .UniqueResult<double>();

            return salesValue;
        }
    }
}